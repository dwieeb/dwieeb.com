---
layout: page
title: Friends
extra_css:
    - friends.css
---

{% include friend.html name="Jeremy Hemberger" url="http://www.jeremyhemberger.com" title="Biologist, Climber, and Photographer" %}
{% include friend.html name="Michael Imhoff" url="http://flickr.com/photos/mwimhoff" title="Pilot and Photographer" %}
{% include friend.html name="Mark Feltner" url="http://blog.feltnerm.com/about/" title="Soccer Dude and Polyglot" %}
