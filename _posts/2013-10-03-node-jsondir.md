---
layout: post
title: node-jsondir
---
I needed a way to specify a file structure schema in JSON but also have a
utility that created said structure. Not seeing anything on NPM, I developed a
package to do just that.

From `README.md` on the [github project page](https://github.com/dwieeb/node-jsondir):

> Doing large asynchronous file operations in Node is simply a pain in the ass.
> Callbacks nest and nest and nest until suddenly you're ten or twenty levels
> of indentation deep and you have no idea what the hell is going on. JSONdir
> helps alleviate your I/O stress by giving you simple and familiar tools to do
> large amounts of work efficiently.

Please use all of github's features for usage, issues, etc.
