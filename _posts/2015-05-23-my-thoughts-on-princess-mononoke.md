---
layout: post
title: My Thoughts on Princess Mononoke (1997)
disqus: y
---

<img src="/images/princess-mononoke.jpg" alt="Princess Mononoke (1997)" style="display: block; margin: 0 auto;">

Let me preface by saying this is my first Hayao Miyazaki film and that I
typically stay clear of animes.

Animated in the nineties, nearing the end of the hand-drawn animation era,
*Princess Mononoke* is a hybrid of animation. Like many of its time, most is
hand-drawn, but some of the more complicated pieces, such as the worm-like
demon flesh, is computer animated. This time period interests me because I love
hand-drawn animations, but they sometimes lack the ability to have special
effects such as this. For example, in *The Lion King* (1994)--the scene in the
gorge with the wildebeest stampede--every wildebeest was computer animated. The
lions and everything else, however, were not. It was a brief window in time at
the twilight of hand-drawn animation that really harmonizes with me.

My general complaint with animes is that there is usually an obvious laziness
to their animation, including but certainly not limited to the lip
asynchronicity, which is weird to see because I watch foreign films exclusively
in their original audio track with subtitles. Thus, said laziness I believe
spreads to other areas of the film such as plot, character development,
purpose, etc. There seems to be, from what I've observed, a general preference
of quantity over quality. I know it is likely due to the money shoved into a
film for production. I know I am spoiled by Disney and the amount of money they
put into movies; I am trying not to be.

In any case, this movie was different than most animes I've seen. There was an
obvious passion put into the animation and into the artwork. The animations
were smooth and not the typical flat-faced expressionlessness I'm used to in
animes. The backdrops and stills were gorgeous and immediately sucked me into
their world. There was something about a boy riding through vast, green,
wind-swept fields on an antelope that was captivating and that brought out a
strong sense of freedom and adventure.

The film takes place somewhere between the 12th and 16th century, which is
during an age of mythology when primitive forms of modern weapons and industry
are beginning to take form. Stories from this time period reveal countless
mention of mythological beasts and spirits. I looked them up. Every
"supernatural" being in the film can be categorized into ancient Japanese
mythological creatures or spirits. Supernaturality in films generally annoys
me, but mythology does not. In this case, I was intrigued to learn a piece of
Japanese mythology while watching. It is also important to note that the end of
mythology (the end of forest spirits and gods among us) comes with the ultimate
industrialization of the human race. This is a clear theme throughout the
movie, said literally by Jiko-bo near the end:

> "The thirst to possess both heaven and earth is what makes us human."

There are also no clear agents of good or evil, which adds realism. Lady Eboshi
is presumed evil, and indeed she is the antagonist, but really she is acting as
any leader of people has or will. Her people respect and adore her, she cares
for the diseased and defends her town from attacks by country samurai and gods
alike. Ashitaka doesn't set out to save the world, he sets out to save himself.
Miyazaki does not want him to be portrayed as an obvious hero. Even the Deer
God, mostly perceived as apathetic just like the forest (and in a macro sense
the natural world), gives life and takes it away. There is a profound deepness
to the film because of that, which I loved.

Although *Princess Mononoke* is meant as a warning, the film ends on a somewhat
high note. Eboshi realizes only after shooting the Deer God's head off that
Irontown should start over and be a "good village." Everybody gets peace and
presumably harmony between Irontown and the forest, which is what everyone was
really fighting for, ironically. We really want Ashitaka and Yakul to join San
and the Moro clan, or vice versa, but he has his life and she has hers. Being
together would mean that one of them has sacrified who they are. Perhaps
occasional visits, romantic or not, is best.
