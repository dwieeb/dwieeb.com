---
layout: post
title: jquery-textrange
---
This jQuery plugin was written by me a few months ago, I just hadn’t gotten around to adding a link to it on my site. If you need a jQuery plugin for messing with the location of selected text in an input field or textarea, this plugin’s for you. You can find the project [on github](https://github.com/dwieeb/jquery-textrange).
