---
layout: post
title: SSH Keys - Linux to Linux
disqus: y
---
I wrote this because I'm frustrated with how other people overcomplicate this.
Skip to the commands if you want the good stuff.

The idea of SSH keys may be a tough idea to grasp for some people. It
definitely was to me when I got started with Linux.

In a nutshell, SSH keys are an alternative authentication method to login to a
remote server. They are faster and more secure than using your password. The
initial setup process can be tedious, though.

> <span class="title">Storytime:</span>
> Imagine, for the sake of comprehension, a door which can only be opened by
> entering code into a keypad that is either so simple that other people can
> guess it or so complex that it is essentially unmemorable. That's basically
> you using your password. The better way is to have a recognizable face and a
> friendly security guard that not only lets you through the door, but holds
> the door for you! That's basically you and your SSH key interacting with the
> SSH daemon on the server.

The process of generating SSH keys creates two SSH keys, referred to the
"public" and "private" key. There are two of them because the authentication
algorithm compares the two keys upon connection. You must take care not to let
others get a hold of your private key. However, you can put the public key
anywhere you want without any security concerns. Sometimes, I send them as
postcards.

> <span class="title">Hint:</span>
> Before we move on, you should have a directory `~/.ssh` chmoded to `700` on
> your local computer and the remote server.

{% highlight bash %}
dwieeb@local $ cd ~/.ssh
# Linux looks in the .ssh folder for authorized SSH keys.
dwieeb@local $ ssh-keygen -t rsa -C "Enter an identifier such as dwieeb@local"
# You can also use "dsa" instead of "rsa." But, RSA is actually more secure with the default options and is more future-proof.
Generating public/private rsa key pair.
Enter file in which to save the key (/home/dwieeb/.ssh/id_rsa):
# I like to name my keys with the remote server's hostname.
Enter passphrase (empty for no passphrase):
# In short, passphrases are only useful if someone gets a hold of your private key.
# If you are bad with Linux permissions, specify a passphrase.
Enter same passphrase again:
Your identification has been saved in ...
{% endhighlight %}

By now you should see the following:

{% highlight text %}
The key's randomart image is:
+-------------[ RSA 2048]----------------+
|........................................|
|......................====+......,......|
|.........,.........,?II~+??+:...........|
|................I?+???=~??+~=:..........|
|..............IIIII?++++~,=~::,.........|
|..,..........I?II++?+?++~::,:?,.........|
|........,....I???+=~==:::,::,=,~....,...|
|............+?III?=:,::~,::.,+:=,.......|
|.............~II???+::...,:,.I:+.....,..|
|............?=I=?I==::.:..,,.::~,,......|
|............II?=????~::::.:..,?,.,......|
|........,...II~~I++~~:::.:....+:,:......|
|............,I?+??+=~~:,:.....I:?:......|
|.............?::~?=~~~~,:....,?~I.,.....|
|..............I?=+===:~:=..~.,+?I,~.....|
|...............::.==~~77II,,::I+::~,....|
|...................II7=:,:.,,.+?~I:=....|
|.,............,...7+===::,..,,??I?.~,...|
|.......,.........I=~+?::,..,,,~??+:??...|
|.................+++=++:,..,...+7:+?+...|
|............,...~=~=+=+=,::,..,+I:......|
|................~=+==+=+:,,:,::~?:......|
|...............~=+++~=~=~:,.:::...,.....|
|..............,~=:~=~~~~=:.,,,.,........|
+----------------------------------------+
{% endhighlight %}

If it did not generate "Girl with a Pearl Earring" by Johannes Vermeer in ASCII
form, your key's randomart generator has no talent.

Next, on the remote server, ensure a file `~/.ssh/authorized_keys` exists
chmoded to `600`. This is a file that, well, stores the authorized keys. Each
public key (the one with the `.pub` extension) is entered one per line. So make
sure your public key exists somewhere in that file. Here's a one-liner for the
hipster dev in all of us:

{% highlight bash %}
dwieeb@local $ cat test.txt | ssh dwieeb@remote "cat >> ~/.ssh/authorized_keys"
{% endhighlight %}

*That's it!*--Barring some configuration issues. I'll cover some common issues
for troubleshooting:

### Incorrect permissions on files and directories

The SSH daemon is very stringent on permissions. It is this way for your
safety. Files and directories mentioned in this tutorial **must** be chmoded
accordingly. It's okay if you have extra files in your `~/.ssh` directory, but
the permissions of the listed files below must match exactly.

{% highlight text %}
dwieeb@local $ cd ~/.ssh
dwieeb@local $ ls -alh
drwx------  2 dwieeb dwieeb 4.0K Aug  5  2014 .
drwxr-xr-x 75 dwieeb dwieeb 4.0K May 10 23:11 ..
-rw-------  1 dwieeb dwieeb 1.7K Aug  5  2014 id_rsa
-rw-r--r--  1 dwieeb dwieeb  398 Aug  5  2014 id_rsa.pub
{% endhighlight %}

{% highlight text %}
dwieeb@remote $ cd ~/.ssh
dwieeb@remote $ ls -alh
drwx------  2 dwieeb dwieeb 4.0K Jul 28  2013 .
drwxr-xr-x 30 dwieeb dwieeb 4.0K May 10 23:11 ..
-rw-------  1 dwieeb dwieeb 1.8K May 10 22:51 authorized_keys
{% endhighlight %}

### SSH daemon is not configured

On the remote server, the SSH daemon needs to allow public key authentication.
In `/etc/ssh/sshd_config` (Debian/Ubuntu), make sure the following config
values are set accordingly:

{% highlight text %}
PubkeyAuthentication yes
AuthorizedKeysFile %h/.ssh/authorized_keys
{% endhighlight %}

If you are still unable to connect, your SSH daemon may be using `AllowGroups`
or `AllowUsers` to allow only certain groups or users to use SSH. Google these
for more tutorials.

When finished configuring your SSH daemon, make sure to restart it
(Debian/Ubuntu):

{% highlight bash %}
dwieeb@remote $ sudo service ssh restart
{% endhighlight %}

If you're still having problems, you'll need to ask for help. But, not from me.
I already told you more than I know.
