---
layout: post
title: Flask&#58; for the bold and true
disqus: y
---

Here is a brief overview of the presentation I gave at [MadPUG](http://www.meetup.com/MadPUG/) on May 12th, 2016.

<script async class="speakerdeck-embed" data-id="8544ccb6b6694ef5a348b78b57497bae" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>

[Flask](http://flask.pocoo.org/) is a microframework (minimalistic, or
foundational framework) for building web applications in Python. It provides a
solid base of tools on which to build instead of providing a massive API and
all encompassing framework.

It's very lightweight because it is a simple, small framework built on top of
[Werkzeug](http://werkzeug.pocoo.org/), a WSGI utility library. WSGI is how
many Python apps talk to the web. Django speaks WSGI. Flask speaks WSGI.

Django is a solid choice for building web applications. Many design decisions
are already made for you, making it easy to use familiar, cohesive tools to get
up and running in no time. Django code is often monolithic (meaning all the
code to run your app lies in one repository, instead of separate packages or
web services), which is another design decision for your app. Monoliths are
often good in the beginning to get the ball rolling, but start to collect
technical debt fast.

Django doesn't make much sense if you know you want to use a different ORM,
form builder, serializer, authentication layer, etc. Because the Django tools
are cohesive, integrating a different library in place of a Django component is
difficult and has side effects that may not be immediately realized.

With Flask, you start from the ground up and add components as needed using
extensions or libraries. In that way, building an app in Flask is a lot like
building a house with LEGOs, whereas with Django, you're already given most of
the house, but as soon as you want to change a component or go against the
"Django Way", you run into difficulties.

Flask uses a templating language called
[Jinja2](http://jinja.pocoo.org/docs/dev/), which is based on Django templates.
It features a clean syntax, template inheritance, and [the power of
blocks](http://jinja.pocoo.org/docs/dev/templates/#base-template).

Flask's boilerplate is literally seven lines of code:

{% highlight python %}
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello World!"

if __name__ == '__main__':
    app.run()
{% endhighlight %}

So much abstraction has been done to provide such a clean API as to create a
fully functional web page in Python in just seven lines of code. It makes me
giddy.

[Download the slides](https://speakerd.s3.amazonaws.com/presentations/8544ccb6b6694ef5a348b78b57497bae/Flask_Presentation.pdf).

### Links in Slides

* [flask.pocoo.org/extensions](http://flask.pocoo.org/extensions)
* [github.com/miguelgrinberg/flasky](https://github.com/miguelgrinberg/flasky)
* [github.com/humiaozuzu/awesome-flask](https://github.com/humiaozuzu/awesome-flask)
