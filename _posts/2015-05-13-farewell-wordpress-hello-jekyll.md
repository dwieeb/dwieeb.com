---
layout: post
title: Farewell Wordpress, Hello Jekyll!
disqus: y
---

I figured I'd give Jekyll a try and see how I liked it. I have to say, I'm
impressed. In just a short weekend, I was able to fork muan's [scribble
theme](https://github.com/muan/scribble) theme and get my Wordpress site ported
over to a pile of HTML, SCSS, and Markdown files.

> For those of you who are unaware, [Jekyll](http://jekyllrb.com/) is one of
> the more popular static site generators that help rethink how blogs are made.
> It's great for developers--and free to host if you use [GitHub
> pages](https://pages.github.com/) (I am not).

## Things I like
* "Get up and running in *seconds*"--This is true, however I'd recommend
  starting with a base theme that gets you started even faster. Take a look at
  [poole](https://github.com/poole/poole) or
  [Jekyll-Bootstrap](https://github.com/plusjade/jekyll-bootstrap). If you want
  a fully completed theme that you then modify or leave alone, there's
  [jekyllthemes.org](http://jekyllthemes.org/) and [the Themes wiki page on
  Jekyll's project page](https://github.com/jekyll/jekyll/wiki/Themes).
* **Asset pathing is easy**. For images, CSS, and Javascript, wherever you put them
  is wherever they end up.
* **Tool familiarity**. Who wants to log in and use a WYSIWYG editor to write
  up your latest hackeries? *groan*. Wordpress ended up frustrating me and demotivating
  me enough to give up on blogging, essentially. For developers using Jekyll,
  all they need to do is open up their favorite editor and write a clean bit of
  Markdown and voil&agrave;!
* **HTML when needed**. Markdown is nice. I like it. But, with Jekyll, <span
  style="font-size: 200%; color: tomato;">I</span> <span style="font-size:
  140%; color: blue; vertical-align: top;">can</span> <span style="font-size:
  180%; color: green; vertical-align: middle;">still</span> <span
  style="font-size: 140%; color: salmon;">use</span> <span style="font-size:
  230%; color: red; vertical-align: bottom;">HTML</span> <span
  style="font-size: 150%; color: hotpink;">when</span> <span style="font-size:
  190%; color: indigo;">I</span> <span style="font-size: 130%; color: cyan;
  vertical-align: top;">need</span> <span style="font-size: 150%; color:
  violet;">to</span> <span style="font-size: 200%; color: olive;">!</span>
* **Front Matter**. Front Matter is a YAML block that lets you define exactly
  what kind of thing you're writing. You can even make up your own front matter
  and have your templates behave differently depending on that front matter.

This allowed me to include extra CSS files on my posts and pages, i.e.:

    ---
    layout: page
    title: Friends
    extra_css:
        - friends.css
    ---

Then, in my `_includes/head.html`:

{% raw %}
    {% for css_name in page.extra_css %}
        <link href="/stylesheets/{{ css_name }}" rel="stylesheet" type="text/css" />
    {% endfor %}
{% endraw %}

* **Versioned in git**. (Or your favorite VCS.) Everything is plain text, so
  you can version your blog in git from the start. This site is versioned
  [here](https://gitlab.com/dwieeb/dwieeb.com).

## Things that need work
* So... unbeknownst to me, I was using Jekyll 1, which apparently didn't have
  the `slugify` filter. I was following the current documentation at the time,
  but for the life of me I could not get `slugify` to work. Jekyll didn't tell
  me that there was an unknown filter being used in Liquid, and it still
  doesn't. =(

## Next steps
* I want to learn how Jekyll handles multiple environments. I essentially wrote
  this post on "production"--everyone can see my post as I work on it.
* I think Jekyll can handle SCSS files and Coffeescript files by default in
  version 2? Something to look into. Right now I use rake to spawn the `scss`
  and `coffee` watchers, as well as Jekyll.
* I want to look into opt-in HTTP/2 for even further speedup with modern
  browsers. I run apache 2.4 on Debian. There may be a follow-up article
  explaining how I get this to work, if I do.
