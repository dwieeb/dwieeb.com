---
layout: page
title: About
---

<img src="/images/daniel.jpg" alt="Dan Imhoff" style="display: block;">

Hello! I am a Wisconsin native living in the beautiful city of Madison. I work
at [Ionic](http://ionic.io/), developing the Ionic Cloud, a BaaS (mobile
backend as a service) that works perfectly with the [Ionic
Framework](http://ionicframework.com/). You can usually find me at a variety of
Madison meetups, such as [Big Data
Madison](http://www.meetup.com/BigDataMadison/),
[MadPUG](http://www.meetup.com/MadPUG/), [MadJUG](http://www.wjug.org/madjug/),
and [MadJS](http://www.meetup.com/MadisonJS/).

I like photography (see [my pictures](https://photos.dwieeb.com)), astronomy,
card games, wildlife, biking, and LEGOs.
